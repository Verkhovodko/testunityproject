﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PayMethod : MonoBehaviour 
{

	public enum PayMethods
	{
		Cash,
		Card,
		Trade,
		Refound,
		Nothing
	}
	public abstract int GetPaid(int totalMoney, int listCount);

	public virtual PayMethods Method(string _playerMoney)
	{
		switch (_playerMoney)
		{
			case "cash":
				return PayMethods.Cash;
			case "card":
				return PayMethods.Card;
			case "trade":
				return PayMethods.Trade;
			case "refound":
				return PayMethods.Refound;
			default:
				return PayMethods.Nothing;
		}
	}

	public virtual int Price(int _lists)
		{
			switch (_lists)
			{
				case 15:
					return 100;
				case 50:
					return 200;
				case 200:
					return 500;
				default:
					return 0;
			}
		}
}

