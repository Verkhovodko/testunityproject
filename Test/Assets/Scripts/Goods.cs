﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Goods : MonoBehaviour
{
	public abstract void Description();
	public int listsCount;

	public virtual int Parers(string paper)
	{
		switch (paper)
		{
			case "sheet":
				return listsCount = 15;
			case "notebook":
				return listsCount = 50;
			case "book":
				return listsCount = 200;
			
			default:
				return listsCount = 0;
		}
	}
}
